# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SaleManagementModel(models.Model):
    _name = 'sm.model'

    id = fields.Integer()
    name = fields.Char()
    manufacturer = fields.Many2one('sm.manufacturer')
