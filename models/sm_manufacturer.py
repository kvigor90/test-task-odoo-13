# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SaleManagementManufacturer(models.Model):
    _name = 'sm.manufacturer'

    id = fields.Integer()
    name = fields.Char()
