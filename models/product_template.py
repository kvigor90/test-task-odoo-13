# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging

_logger = logging.getLogger(__name__)

class ProductTemplateExt(models.Model):
    _name = 'product.template'
    _inherit = 'product.template'

    manufacturer = fields.Many2one('sm.manufacturer')
    model = fields.Many2one('sm.model')
    states_list = [
        ('step1', 'step1'),
        ('step2', 'step2'),
    ]
    state = fields.Selection(states_list)
    product_name = fields.Char(compute='_manufacturer_onchange')

    @api.onchange('manufacturer', 'model')
    def _manufacturer_onchange(self):
        # form and wizard
        if self.manufacturer != self.model.manufacturer:
            self.model = False
        # wizard
        if self.env.context.get('quick_create_form', False):
            self.name = '%s%s%s' % (
                self.manufacturer.name if self.manufacturer.name else '',
                '-' if self.manufacturer.name and self.model.name else '',
                self.model.name if self.model.name else ''
            )
            self.product_name = self.name

    def next_step(self):
        self.state = 'step2'
        view_id = self.env.ref(
            'sale_management_ext.sale_management_ext_quick_create_product_template_form')
        return {
            'name': self.name,
            'type': 'ir.actions.act_window',
            'view_type': 'form', 
            'view_mode': 'form',
            'res_model': 'product.template',
            'res_id': self.id,
            'view_id': view_id.id,
            'target': 'new',
            'context': {},
        }

    def save_record(self):
        return True
#