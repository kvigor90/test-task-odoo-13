# -*- coding: utf-8 -*-
{
    'name': "sale_management_ext",

    'summary': """
        Test Task""",

    'description': """
        To contact the author, write to the email k*****90@gmail.com.
    """,

    'author': "k*****90@gmail.com",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'sale_management',
        'sale',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/sm_manufacturer.xml',
        'views/sm_model.xml',
        'views/product_template.xml',
        'views/sm_menu.xml',
    ],
}
